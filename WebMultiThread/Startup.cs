﻿using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebMultiThread.Startup))]
namespace WebMultiThread
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            GlobalConfiguration.Configuration.UseSqlServerStorage("WebConn");

            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}
