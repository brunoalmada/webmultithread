﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebMultiThread.Controllers
{
    public class ProcessController : ApiController
    {
        public IHttpActionResult Get()
        {
            try
            {
                Debug.WriteLine("Well, I will call a background process");
                
                BackgroundJob.Enqueue(() => this.StartProcess());

                Debug.WriteLine("Yes, I called. You can continue using our nice web application!");

                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        public void StartProcess()
        {
            new ProcessJobs.ProcessJob().PleaseProcessThis();
        }
    }
}
