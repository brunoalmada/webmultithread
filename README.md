# README #

We have a web application, and we need to call a background process, which can take minutes, or even hours. So why don't we delegate it to Hangfire? So, here we are! Enjoy!

Do you have an idea on how to improve this solution? Great, I'm looking forward for your ideas: just fork my repository and send me a pull request :D

### What is this repository for? ###

* Do BackgroudWork things without stopping or freezing your web application
* Version 1

### How do I get set up? ###

* This project uses [Hangfire](http://hangfire.io/), Owin, Newtonsoft Json. It is all set up with Nuget :)
* There is a file database (App_Data/LocalData.mdf) for this project.

### Who do I talk to? ###

* Bruno Almada
* * brunoha@gmail.com
* * [My LinkedIn profile](http://linkedin.com/in/brunoalmada)
* * [My Xing profile](https://www.xing.com/profile/Bruno_HeitzmannAlmada)
* * [about.me](https://about.me/brunoha)